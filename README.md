[![pipeline status](https://gitlab.sorbonne-paris-nord.fr/11931512/naive-file-database/badges/master/pipeline.svg)](https://gitlab.sorbonne-paris-nord.fr/11931512/naive-file-database/-/commits/master)

[![coverage report](https://gitlab.sorbonne-paris-nord.fr/11931512/naive-file-database/badges/master/coverage.svg)](https://gitlab.sorbonne-paris-nord.fr/11931512/naive-file-database/-/commits/master)


A simple private package to manage loading and saving data from and to a file. 